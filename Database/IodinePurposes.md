#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Videos
* Iodine and Why You Need It. ( Part [1](https://www.youtube.com/watch?v=rj-dwWsm8D0&list=RDrj-dwWsm8D0&index=1), [2](https://www.youtube.com/watch?v=hZ_Ls6lJBQU), [3](https://www.youtube.com/watch?v=B0gFs1FevYQ), [4](https://www.youtube.com/watch?v=qzng7ddZEGs), [5](https://www.youtube.com/watch?v=o_ZvmuCwk2o) )
  * On the importance of iodine,
  * The misinformation about its dosing,
  * The recommendations of an authority on the subject.