#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  

# Studies
* [Whole grain consumption and risk of cardiovascular disease, cancer, and all cause and cause specific mortality: systematic review and dose-response meta-analysis of prospective studies](/ArchivedStudies/Whole_grain_consumption_and_risk_of_cardiovascular_disease_cancer_and_all_cause_and_cause_specific_mortality_systematic_review_and_dose-response_meta-analysis_of_prospective_studies.html)
  * Metastudy with sources from PubMed and Embase searched
    * from their inception (1966 and 1947, respectively) to 31 May 2014 and later updated the search to 3 April 2016
  * To quantify the dose-response relation between consumption of whole grain and specific types of grains and the risk of particular ailments.
  * Provides a good resource for showing why one should consume higher quantities of whole grains.
  * Does not make any claim to have compared patients who consume no grains and whole grains (however, "greater intake" from any base was what findings were from)
    * I've seen this study misrepresented to claim whole grain intake increases are benefitial, but one should note that this could be due to a decrease in processed grain consumption. 
    * This leaves "no grain or whole grain" undetermined as the author did not mention abstinence of grain consumption.
  * If it was missed making me inaccurate, please inform me.

# Videos
* [Food-Dependent Exercise-Induced Anaphylaxis—Importance of Omega-5 Gliadin and HMW-Glutenin as Causative Antigens for Wheat-Dependent Exercise-Induced Anaphylaxis—](https://www.sciencedirect.com/science/article/pii/S1323893015307334)
  * Discoveries for wheat's allergenic affects
  * Effects of wheat consumption with other factors such as exercise and aspirin-intake.
* [Is gliadin really safe for non‐celiac individuals?](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1954879/)
  * More than only celiac patients are affected by gliadin 
  * Tests of gliadin on non-celiac individuals. 
* [The dietary intake of wheat and other cereal grains and their role in inflammation](https://www.ncbi.nlm.nih.gov/pubmed/23482055https://www.ncbi.nlm.nih.gov/pubmed/23482055)
  * Cereal grains contain "anti-nutrients".
  * The "anti-nutrients" can elicit dysfunction and disease.
  * Discussion on consumption of wheat can contribute to chronic inflammation and autoimmune diseases.
  * Discussion on consumption of wheat can initiate a pro-inflammatory immune response.
* [The Characterization of the Repertoire of Wheat Antigens and Peptides Involved in the Humoral Immune Responses in Patients with Gluten Sensitivity and Crohn's Disease](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3658803/)
  * Patients were tested against wheat antigens and peptides.
  * Measurements of IgG and IgA antibodies against this array of wheat peptides and antigens 
    * can enhance the sensitivity and specificity of serological assays for gluten sensitivity and celiac disease 
    * may detect silent celiac disease or its overlap with inflammatory bowel disease
* [Whole and Refined Grain Intakes Are Related to Inflammatory Protein Concentrations in Human Plasma](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2821887/)
  * Examines association of whole grains and refined grain intake 
 
